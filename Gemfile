source 'https://rubygems.org'

gem 'rails', '4.0.0'

# Database
gem 'sqlite3'
# gem 'pg'

# CSS
gem 'sass-rails', '~> 4.0.0'
gem 'bootstrap-sass'
gem 'bootswatch-rails'

# Javascript
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'


group :development do
  gem 'better_errors'
  gem 'quiet_assets'
  gem 'puma'
end


# These are far more gems than you'll need at first. I just copied this part of my Gemfile from another project that I wrote quite a few tests for. Feel free to research these gems at your discretion. They're all very commonly used, but you only NEED 'rspec-rails' to do the testing we do in class.

group :development, :test do
  gem 'rspec-rails', '~> 3.0.0.beta'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers', :git => 'git://github.com/thoughtbot/shoulda-matchers.git'
  gem 'shoulda-callback-matchers', ">=0.3.0"
  gem 'faker'
  gem 'guard'
  gem 'guard-rspec'
  gem 'rb-fsevent', "~> 0.9.1"
  gem 'webrat'
end

group :test do
  gem 'email_spec'
  gem 'database_cleaner'
  gem 'capybara'
end