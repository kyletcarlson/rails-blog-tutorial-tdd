class CommentsController < ApplicationController

  # See posts controller. This time, we only force the name/password when they try to delete a comment.
  http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy

  def create
    # A Comment belongs to only one Post, so find the Post this Comment will be attached to
    @post = Post.find(params[:post_id])
    # Create a Comment that's linked to the Post and fill it with the commenter's name and his/her comment
    @comment = @post.comments.create(params[:comment].permit(:commenter, :body))
    redirect_to post_path(@post)
  end

  def destroy
    # A Comment belongs to only one Post, so find the Post this Comment will be attached to
    @post = Post.find(params[:post_id])
    # Grab the comment we're trying to delete
    @comment = @post.comments.find(params[:id])
    # Delete the comment
    @comment.destroy
    # Send the user back to the Post you deleted the Comment from.
    redirect_to post_path(@post)
  end
end