class PostsController < ApplicationController

  # Force the user to type the correct username and password to do anything in the Posts controller except for viewing the index or a single post. Feel free to change the name/password to something else.
  http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]

  def index
    # Create a @posts instance variable and fill it with all the posts in the database
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def show
    # Create a @post instance variable, fetch the Post from the database with the id in the params hash, and fill it with the Post pulled from the DB.
    @post = Post.find(params[:id])
  end

  def edit
    # Create a @post instance variable, fetch the Post from the database with the id in the params hash, and fill it with the Post pulled from the DB.
    @post = Post.find(params[:id])
  end

  def update
    # Create a @post instance variable, fetch the Post from the database with the id in the params hash, and fill it with the Post pulled from the DB.
    @post = Post.find(params[:id])
   
    if @post.update(post_params)
      # If @post updated correctly, redirect the user to it
      redirect_to @post
    else
      # Otherwise, send the user back to the form to try again
      render 'edit'
    end
  end

  def create
    # Create an instance variable that can be used in the view
    # Create a new instance of the Post model with the params (title & body) that came from the form
    @post = Post.new(post_params)

    # Try to actually save the post to the database instead of just holding it in memory
    if @post.save
      # If @post saved correctly, redirect the user to it
      redirect_to @post
    else
      # Otherwise, send the user back to the form to try again
      render 'new'
    end
  end

  def destroy
    # Create a @post instance variable, fetch the Post from the database with the id in the params hash, and fill it with the Post pulled from the DB.
    @post = Post.find(params[:id])
    # Nuke it!
    @post.destroy
    # Go back to the listing of all the posts
    redirect_to posts_path
  end


  # Methods below 'private' can only be acessed by methods inside this controller. There's no way to visit the route /posts/post_params. 
  private

    # Good ol' Rails 4 Strong Parameters...
    # http://edgeguides.rubyonrails.org/action_controller_overview.html#strong-parameters
    def post_params
      # Remember how we created a new Post from the params? This line tells Rails to only allow the title and body to be set from the params. It's a major security improvement, but it seems wonky at first.
      params.require(:post).permit(:title, :text)
    end

end
