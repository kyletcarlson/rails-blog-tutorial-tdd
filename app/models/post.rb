class Post < ActiveRecord::Base
  # Posts have comments, and tell Rails to automagically delete all comments for a post when it is deleted
  has_many :comments, dependent: :destroy

  # Only let Posts be created that a) have a title, and b) the title has to have at least 5 characters
  validates :title, presence: true,
                    length: { minimum: 5 }


  # def popular?
  #   # if self.comments.count >= 5
  #   #   return true
  #   # else
  #   #   return false
  #   # end

  #   # This line is the same as the five lines above. It's called the "ternary operator". Google it.
  #   self.comments.count >= 5 ? true : false
  # end

  # def long_title?
  #   # http://ruby-doc.org/core-2.0.0/String.html#method-i-split
  #   title_as_word_array = self.title.split(" ")

  #   # Another instance of the ternary operator. It rocks.
  #   title_as_word_array.size > 5 ? true : false
  # end
end
