# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post do
    title "Test Blog Post"
    text "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui lorem, lacinia vel lectus vel, facilisis bibendum nisl. Suspendisse at libero sed turpis dapibus malesuada vel eu lectus. Aenean volutpat cursus iaculis. Nullam sed gravida ante. Mauris rhoncus nunc vitae tempus lacinia. Nunc sit amet tempor justo."
  end
end
