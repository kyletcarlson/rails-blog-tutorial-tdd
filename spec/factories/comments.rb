# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    commenter "User 1"
    text "Lorem ipspum dolor amet, blah, blah, blah..."
  end
end
