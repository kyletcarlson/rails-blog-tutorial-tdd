require 'spec_helper'

describe Post do

  # It's a good idea to test any factories you're using in your specs just to make sure you're actually testing what you're intending to.
  it "has a valid factory" do
    # Using the shortened version of FactoryGirl syntax.
    # Add:  "config.include FactoryGirl::Syntax::Methods" (no quotes) to your spec_helper.rb
    expect(build(:post)).to be_valid
  end

  # Lazily loaded to ensure it's only used when it's needed.
  # RSpec tip: Try to avoid @instance_variables if possible. They're slow.
  let(:valid_post) { build(:post) }

  # It's ALWAYS a good idea to write tests for your validations. They're pretty easy & very important to the proper functioning of your models. Do them the verbose way (below), and then look at the shoulda-matchers gem to see how incredibly easy it makes testing validations & other aspects of your models.
  describe "validations" do

    describe "title" do
      it "requires a title" do
        invalid_post = Post.new(title: nil, text: "Lorem ipsum dolor amet...")
        expect(invalid_post.save).to eq(false)
      end

      it "must be at least 5 characters" do
        invalid_post = Post.new(title: "1234", text: "Lorem ipsum dolor amet...")
        expect(invalid_post.save).to eq(false)

        valid_post = Post.new(title: "12345", text: "Lorem ipsum dolor amet...")
        expect(valid_post.save).to eq(true)
      end
    end
  end

  # Look in /app/models/post.rb for these new methods
  describe "instance methods" do
    describe ".popular?" do
      it "returns false if less than 5 comments" do
        expect(valid_post.popular?).to eq(false)
      end

      # it "returns true if at least 5 comments" do
      #   pending("Write this test on your own. This is the way to add a message describing why the test is pending. It's helpful for yourself & others who may look at your code.")
      # end
    end

    # describe ".long_title?" do
    #   # Remember, the valid_post has the title "Test Blog Post"
    #   it "returns false if the title is less than 6 words" do
    #     expect(valid_post.long_title?).to eq(false)  # since we only have 3 characters
    #   end

    #   it "returns true if the title is longer than 5 words" do
    #     long_titled_post = Post.new(title: "one two three four five six seven eight", text: "asdf")
    #     expect(long_titled_post.long_title?).to eq(true)
    #   end
    # end
  end
end
