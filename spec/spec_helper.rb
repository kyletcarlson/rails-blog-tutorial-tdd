# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] = 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'shoulda/matchers/integrations/rspec' # after require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'
require 'database_cleaner'
require 'email_spec'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  config.include(EmailSpec::Helpers)
  config.include(EmailSpec::Matchers)

  # Allows for shorter FactoryGirl syntax. 
  config.include FactoryGirl::Syntax::Methods
  
  # Run specs in random order to surface order dependencies. 
  config.order = "random"

  # config.treat_symbols_as_metadata_keys_with_true_values = true
  # config.filter_run_excluding :slow

  # Include the Capybara DSL so that specs in spec/requests still work.
  config.include Capybara::DSL
  # Disable the old-style object.should syntax.
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  Capybara.default_host = 'localhost:3000'

  config.use_transactional_fixtures = false

  config.before(:suite) do
    # Holy shit!  17sec -> 6sec with these two settings
    # http://platformonrails.wordpress.com/2013/02/27/a-path-to-fast-and-honest-tests-rspec-and-database-cleaner/
    # Do truncation once per suite to vacuum for Postgres
    DatabaseCleaner.clean_with :truncation
    # Normally do transactions-based cleanup
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end