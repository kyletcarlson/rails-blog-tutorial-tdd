# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Railsblog::Application.config.secret_key_base = '8e2acfd5658704ca22db81eff1a9e67922ce94375f6a4dfb97151490ba379d260ca69d8575c1925bbd3ef43ede567499e8e23e6973e94d01aede6497174bfbaf'
